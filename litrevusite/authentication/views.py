from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth import login
from . import forms


class SignupPage(View):
    """
    View for manager user signup.

    Attributes:
    - form_class(class): Form class for user signup.
    - template_name(str): Template used for rendering signup page.

    Methods:
    - get(request): Manage GET request and render signup page.
    - post(request): Manage POST request, save new user and redirect it on
    home page if form is valid.
    """
    form_class = forms.SignupForm
    template_name = 'authentication/signup.html'

    def get(self, request):
        """
        Manage GET request and render signup page.

        Parameters:
        request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Render signup page with form.
        """
        form = self.form_class()
        return render(request, self.template_name,
                      {'form': form})

    def post(self, request):
        """
        Manage POST request, save new user and redirect it on home page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponseRedirect: Redirect on home page if form is valid, else,
        render singup page.
        """
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
        return render(request, self.template_name,
                      {'form': form})

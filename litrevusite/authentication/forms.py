from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django import forms


class SignupForm(UserCreationForm):
    """
    Form of user signup.

    This form extends 'UserCreationForm' for include customize styling and
    placeholder attributes for username and password field.

    Attributes:
    - username(CharField): Field for entering username with custom
    placeholder and class attribute.
    - password1(CharField): Field for entering password with custom
    placeholder and class attribute.
    - password2(CharField): Field for confirming password with custom
    placeholer and class attribute.

    Methods:
    - __init__(*args, **kwargs): Initilize form and set custom labels for
    fields.
    - Meta: Meta class specify model and fields for form.
    """
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': "Nom d'utilisateur",
            'class': 'form-control',
            'aria-label': "Nom d'utilisateur"
        }
    ))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'placeholder': 'Mot de passe',
            'class': 'form-control',
            'aria-label': 'Mot de passe'
        }
    ))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'placeholder': 'Confirmation du mot de passe',
            'class': 'form-control',
            'aria-label': 'Confirmation du mot de passe'
        }
    ))

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = ''
        self.fields['password1'].label = ''
        self.fields['password2'].label = ''

    class Meta(UserCreationForm.Meta):
        model = get_user_model()
        fields = ('username',)


class AuthenticationForm(AuthenticationForm):
    """
    Form of user authentication.

    This form extends 'AuthenticationForm' for include customize styling and
    placeholder attributes for username and password fields.

    Attributes:
    - username(CharField): Field for entering username with custom placeholder
    and class attribute.
    - password(CharField): Field for entering password with custome
    placeholder and class attribute.

    Methods:
    - __init__(*args, **kwargs); Initialize form and set custom labels for
    fields.
    """
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': "Nom d'utilisateur",
            'class': 'form-control',
            'aria-label': "Nom d'utilisateur"
        }
    ))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'placeholder': 'Mot de passe',
            'class': 'form-control',
            'aira-label': 'Mot de passe'
        }
    ))

    def __init__(self, *args, **kwargs):
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = ''
        self.fields['password'].label = ''

from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)


class UserManager(BaseUserManager):
    """
    Custom manager for User model.

    This manager provides methods to create users and superusers and retrieves
    it by their natural keys.

    Methods:
    - create_user(username, password, **extra_fields):
    Create and save user with given username, password and extra fields.

    - create_superuser(username, password, **extra_fields):
    Create and save super user with given username, password and extra fields.

    - get_by_natural_key(username): Retrieves user instance by username.
    """
    def create_user(self, username, password=None, **extra_fields):
        if not username:
            raise ValueError("Vous devez indiquer un nom d'utilisateur.")
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(
                "Le super user doit avoir accès au site d'administration")
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(
                'Le super user doit avoir toutes les permissions')

        return self.create_user(username, password, **extra_fields)

    def get_by_natural_key(self, username):
        return self.get(username=username)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model for registrated user.

    This model extends 'AbstractBaseUser' and 'PermissionsMixin' for customize
    the default user model.

    Attributes:
    - username(str): Charfield for username.
    - is_staff(BooleanField): Define if user is allow to access admin site.
    - is_active(BooleanField): Define if user account is active.
    - follows(ManyToManyField): Define users followed by this user.
    - blocked_users(ManyToManyField): Define users blocked by this user.
    - objects: Instance of 'UserManager'.

    Methods:
    - __str__(): Returns string of user instance, displaying username.
    - natural_key(): Returns username of user.
    """
    username = models.CharField(
        max_length=25, verbose_name="Nom d'utilisateur", unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    follows = models.ManyToManyField(
        'self',
        symmetrical=False,
        verbose_name='followers'
    )
    blocked_users = models.ManyToManyField(
        'self',
        symmetrical=False,
        blank=True,
        verbose_name="Utilisateurs bloqués",
        related_name='blocked_set'
    )

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    def natural_key(self):
        return (self.username)

"""
URL configuration for litrevusite project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView
from django.conf.urls.static import static
from django.conf import settings
from authentication.forms import AuthenticationForm

import authentication.views
import litreview.views

"""
URL patterns for applications.

Each URL pattern maps a URL path to a specific view function or class-based
view.

URL Patterns:
- 'admin/' : Admin page.
- '' (index): Landing page of application.
- 'home/': Home page of user connected.
- 'signup/': Signup page.
- 'logout/': Logout page for end user session.
- 'ticket/create/': Page for create an ticket.
- 'ticket/<int:ticket_id>/update': Page for modify an ticket.
- 'ticket/<int:ticket_id>/delete': Page for delete an ticket.
- 'review/create/': Page for create an review.
- 'ticket/<int:ticket_id>/createreview': Page for create review in answering
of an ticket.
- 'review/<int:review_id>/update': Page for modify an review.
- 'review/<int:review_id>/delete': Page for delete an review.
- 'follows/': Page of displaying followers, followed users, blocked users.
- 'unfollow/<int:user_id>/': Page for unfollow user.
- 'posts/<str:username>/': Page of user posts based on username.
- 'userblocked/<int:user_blocked_id>/': Page for blocked user.
- 'unblock-user/<int:user_unblocked_id>/': Page for unblocked user.

For more informations on each view, you can view efinitions in views.py
"""
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LoginView.as_view(
        template_name='authentication/accueil.html',
        redirect_authenticated_user=True,
        authentication_form=AuthenticationForm
    ), name='accueil'),
    path('home/', litreview.views.Home.as_view(), name='home'),
    path('signup/', authentication.views.SignupPage.as_view(), name='signup'),
    path('logout/', litreview.views.logout_view, name='logout'),
    path('ticket/create/', litreview.views.TicketCreate.as_view(),
         name='ticket-create'),
    path('ticket/<int:ticket_id>/update',
         litreview.views.TicketUpdate.as_view(),
         name='ticket-update'),
    path('ticket/<int:ticket_id>/delete',
         litreview.views.TicketDelete.as_view(),
         name='ticket-delete'),
    path('review/create/', litreview.views.ReviewCreate.as_view(),
         name='review-create'),
    path('ticket/<int:ticket_id>/createreview',
         litreview.views.ReviewAnswer.as_view(),
         name='review-answer'),
    path('review/<int:review_id>/update',
         litreview.views.ReviewUpdate.as_view(),
         name='review-update'),
    path('review/<int:review_id>/delete',
         litreview.views.ReviewDelete.as_view(),
         name='review-delete'),
    path('follows/', litreview.views.FollowUsers.as_view(), name='follows'),
    path('unfollow/<int:user_id>/',
         litreview.views.UnfollowUser.as_view(), name='unfollow'),
    path('posts/<str:username>/',
         litreview.views.Posts.as_view(), name='user-posts'),
    path('userblocked/<int:user_blocked_id>/',
         litreview.views.BlockedUser.as_view(), name='user-blocked'),
    path('unblock-user/<int:user_unblocked_id>/',
         litreview.views.UnblockedUser.as_view(), name='unblock-user')
]
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

from django import forms
from django.contrib.auth import get_user_model

from . import models


User = get_user_model()


class TicketForm(forms.ModelForm):
    """
    Form of creating or editing ticket.

    Methods:
    - __init__(*args, **kwargs): Initialize instance of form and customize
    widget of field image for use widget 'CustomImage' with specific
    attributes.
    - current_image(): Return URL of current image associated with instance
    of ticket if it exist, else, return None.
    """

    class Meta:
        model = models.Ticket
        fields = ['title', 'content', 'image']
        labels = {
            'title': 'Titre',
            'content': 'Description',
            'image': 'Image'
        }

    def __init__(self, *args, **kwargs):
        """
        Initialize TicketForm instance.

        Parameters:
        - *args: Positional arguments
        - **kwargs: Keyword arguments
        """
        super().__init__(*args, **kwargs)
        self.fields['image'].widget = CustomImage(
            attrs={
                'class': 'form-control'
                })

    def current_image(self):
        """
        Retrieve URL of current image associated with instance of ticket.

        Returns:
        - str or None: Return URL of current image associated with instance
        of ticket if it exist, else, return None.
        """
        if self.instance and self.instance.pk:
            return self.instance.image.url
        return None


class CustomImage(forms.ClearableFileInput):
    """
    A custom form widget for manage image file inputs.

    Attributes:
    - template_name(str): Template used to render the widget.
    """
    template_name = 'widgets/custom_image.html'


class ReviewForm(forms.ModelForm):
    """
    Form of creating or editing review.

    Attributes:
    - edit_review(BooleanField): Used for indicate if the form whether to
    editing an exist review.
    """
    edit_review = forms.BooleanField(widget=forms.HiddenInput, initial=True)

    class Meta:
        model = models.Review
        fields = ['title', 'rating', 'content']
        widgets = {
            'rating': forms.RadioSelect(
                choices=[
                    (0, '0'),
                    (1, '1'),
                    (2, '2'),
                    (3, '3'),
                    (4, '4'),
                    (5, '5')
                ]
            )
        }
        labels = {
            'title': 'Titre',
            'rating': 'Note',
            'content': 'Commentaire'
        }


class DeleteTicketForm(forms.Form):
    """
    Form for delete ticket.

    Attributes:
    - delete_ticket(BooleanField): Used for indicate ticket deletion.
    """
    delete_ticket = forms.BooleanField(widget=forms.HiddenInput, initial=True)


class DeleteReviewForm(forms.Form):
    """
    Form for delete review.

    Attributes:
    - delete_review(BooleanField): Used for indicate review deletion.
    """
    delete_review = forms.BooleanField(widget=forms.HiddenInput, initial=True)


class FollowForm(forms.Form):
    """
    Form for following user.

    Attributes:
    - username(CharField): Field for entering username.

    Methods:
    - __init__(*args, **kwargs): Initialize form instance with custom
    attributes for username.
    - clean_username: Verify if the username indicate exist in the User model.
    """
    username = forms.CharField(max_length=150,
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': "Nom d'utilisateur",
                                       'aria-label': "Rechercher utilisateur"}
                                       ))

    def __init__(self, *args, **kwargs):
        super(FollowForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = ''

    def clean_username(self):
        username = self.cleaned_data['username']
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError("Utilisateur non trouvé")
        return username


class BlockUserForm(forms.Form):
    """
    Form for blocking user.

    Attributes:
    - This form not needed to have attributes because it use only for
    submission POST.
    """
    pass


class UnblockedUserForm(forms.Form):
    """
    Form for unblocking user.

    Attributes:
    - This form not needed to have attributes because it use only for
    submission POST.
    """
    pass

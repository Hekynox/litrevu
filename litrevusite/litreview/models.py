from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.conf import settings
from django.contrib.auth import get_user_model
from PIL import Image


User = get_user_model()


class Ticket(models.Model):
    """
    Model of ticket created by a user.

    Attributes:
    - IMAGE_MAX_SIZE(int, int): Max size in pixels for image resize.
    - title(str): Title of ticket.
    - content(str): Content of ticket.
    - image(ImageField): Image attached to the ticket.
    - time_created(DateTimeField): Date and time when the ticket was created.
    - author(ForeignKey): User who created the ticket.

    Methods:
    - resize_image(): Resize attached image to a maximum size defined in
    IMAGE_MAX_SIZE and save it.
    - save(*args, **kwargs): Replace save method to automically resize image
    before saving it.
    """
    IMAGE_MAX_SIZE = (250, 150)
    title = models.CharField(max_length=128)
    content = models.CharField(max_length=8192, blank=True)
    image = models.ImageField()
    time_created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def resize_image(self):
        """
        Resize attached image to a maximum size defined in IMAGE_MAX_SIZE
        and save it.
        """
        image = Image.open(self.image)
        image.thumbnail(self.IMAGE_MAX_SIZE)
        image.save(self.image.path)

    def save(self, *args, **kwargs):
        """
        Replace save method to automically resize image before saving it.
        """
        super().save(*args, **kwargs)
        self.resize_image()


class Review(models.Model):
    """
    Model of review written by user.

    Attributes:
    - ticket(ForeignKey): The Ticket being reviewed.
    - rating(PositiveSmallIntegerField): Rating given to the ticket.
    - title(str): Title of review.
    - content(str): content of review.
    - author(ForeignKey): User who created review.
    - time_created(DateTimeField): Date and time when the review was created.
    """
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    rating = models.PositiveSmallIntegerField(
        # validates that rating must be between 0 and 5
        validators=[MinValueValidator(0), MaxValueValidator(5)])
    title = models.CharField(max_length=128)
    content = models.CharField(max_length=8192, blank=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    time_created = models.DateTimeField(auto_now_add=True)


class UserFollows(models.Model):
    """
    Model of relationship where one user follows another.

    Attributes:
    - follower(ForeignKey): User who is following other user.
    - following(ForeignKey): User who is being followed.
    - follow_date(DateTimeField): Date and time when follow relationship
    was created.

    Meta:
    - unique_together(tuple): Ensure for only one instance exist between an
    user and other user.
    """
    follower = models.ForeignKey(
        User, related_name='following', on_delete=models.CASCADE)
    following = models.ForeignKey(
        User, related_name='followers', on_delete=models.CASCADE)
    follow_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('follower', 'following')

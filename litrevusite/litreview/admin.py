from django.contrib import admin
from litreview import models


class TicketAdmin(admin.ModelAdmin):
    list_display = ('title', 'time_created', 'author')


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('title', 'ticket', 'author', 'time_created')


class FollowsAdmin(admin.ModelAdmin):
    list_display = ('follower', 'following', 'follow_date')


admin.site.register(models.Ticket, TicketAdmin)
admin.site.register(models.Review, ReviewAdmin)
admin.site.register(models.UserFollows, FollowsAdmin)

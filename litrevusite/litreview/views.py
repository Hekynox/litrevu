from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.contrib.auth import logout
from django.contrib.auth import get_user_model
from itertools import chain
from django.db.models import CharField, Value, Q
from django.http import HttpResponseForbidden

from . import forms, models

User = get_user_model()


class Home(LoginRequiredMixin, View):
    """
    View for displaying home page with tickets and reviews.

    Display tickets and reviews of followed users by connected user, this flow
    has sorted by antechronological order.

    Attributes :
    - template_name(str) : Template used for rendering home page.

    Methods :
    - get(request): Manage GET request and render home page with context.
    """
    template_name = 'litreview/home.html'

    def get(self, request):
        """
        Manage GET request and render home page.

        This method retrieves tickets and reviews of followed users by
        connected user, it annotate each element with type of content and
        sorts them by antechronological order.

        Parameters :
        - request(HttpRequest): Request object.

        Returns :
        - HttpResponse: Render home page with context data.
        """
        user = request.user
        followed_users = user.following.values_list('following', flat=True)

        tickets_all = models.Ticket.objects.all()

        review_ticket = {
            ticket.id: models.Review.objects.filter(ticket=ticket).exists()
            for ticket in tickets_all
        }

        ticket = models.Ticket.objects.filter(
            Q(author__in=followed_users) |
            Q(author=user))
        review = models.Review.objects.filter(
            Q(ticket__author__in=followed_users) |
            Q(ticket__author=user) |
            Q(author=user))

        reviews = review.annotate(content_type=Value('REVIEW', CharField()))
        tickets = ticket.annotate(content_type=Value('TICKET', CharField()))

        posts = sorted(
            chain(tickets, reviews),
            key=lambda post: post.time_created,
            reverse=True
        )

        context = {
            'posts': posts,
            'review_ticket': review_ticket,
        }
        return render(request, self.template_name, context=context)


def logout_view(request):
    """
    Log out user currently connected and redirect to page for logged in.

    The view lo out the user associated with the given HttpRequest object then
    redirect to page for logged in.

    Paramaters:
    - request(HttpRequest): Request object.

    Returns:
    - HttpResponseRedirect: Redirect to page for logged in ('accueil') after
    logging out user.
    """
    logout(request)
    return redirect('accueil')


class TicketCreate(LoginRequiredMixin, View):
    """
    View for displaying page to create an ticket.

    Display form must be completed for create an ticket.

    Attributes:
    - form_class(class): form class of creation ticket.
    - template_name(str): Template used for rendering ticket creation page.

    Methods:
    - get(request): Manage GET request and render ticket creation page with
    form.
    - post(request): Manage POST request and save ticket has been create
    and redirect on home page if form is valid.
    """
    form_class = forms.TicketForm
    template_name = 'litreview/ticket_create.html'

    def get(self, request):
        """
        Manage GET request and render ticket creation page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Render ticket creation page with form.
        """
        form = self.form_class()
        return render(request, self.template_name,
                      {'form': form})

    def post(self, request):
        """
        Manage POST request, save ticket and redirect to home page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Rendered ticket creation page with the form if it not
        valid, else, redirect to the home page.
        """
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            ticket = form.save(commit=False)
            ticket.author = request.user
            ticket.save()
            return redirect('home')
        return render(request, self.template_name,
                      {'form': form})


class TicketUpdate(LoginRequiredMixin, View):
    """
    View for diplaying page to modify an ticket.

    Display form must be completed for modify ticket.

    Attributes:
    - edit_form_class(class): form class of ticket.
    - template_name(str): Template used for rendering ticket modify page.

    Methods:
    - get(request, ticket_id): Manage GET request and render ticket modify
    page with form.
    - post(request, ticket_id): Manage POST request and save ticket has been
    modify and redirect on home page if form is valid.
    """
    edit_form_class = forms.TicketForm
    template_name = 'litreview/ticket_update.html'

    def get(self, request, ticket_id):
        """
        Manage GET request and render ticket modify page if user allow to
        modify it, else, denied access of this page.

        Parameters:
        - request(HttpRequest): Request object.
        - ticket_id(int): ID of ticket to be modified.

        Returns:
        - HttpResponse: Render ticket modify page with form.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)
        if ticket.author != request.user:
            return HttpResponseForbidden(
                "Accés refusée, vous n'êtes pas l'auteur de cette critique.")
        edit_form = self.edit_form_class(instance=ticket)
        context = {'edit_form': edit_form}
        return render(request, self.template_name, context=context)

    def post(self, request, ticket_id):
        """
        Manage POST request, save modification of ticket, and redirect
        to home page.

        Parameters:
        - request(HttpRequest): Request object
        - ticket_id(int): ID of ticket to be modified.

        Returns:
        - HttpResponse: Rendered ticket modify page with the form if it not
        valid, else, save modification of ticket object then redirect to home
        page.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)

        edit_form = self.edit_form_class(
            request.POST, request.FILES, instance=ticket)
        if edit_form.is_valid():
            edit_form.save()
            return redirect('home')

        context = {'edit_form': edit_form}

        return render(request, self.template_name, context=context)


class TicketDelete(LoginRequiredMixin, View):
    """
    View for displaying page to delete ticket.

    Display ticket which will be deleted.

    Attributes:
    - delete_form_class(class): form class to delete ticket.
    - template_name(str): Template used for rendering ticket deleted page.

    Methods:
    - get(request, ticket_id): Manage GET request and render ticket deleted
    page with form.
    - post(request, ticket_id): Manage POST request and delete ticket then to
    redirect to home page.
    """
    delete_form_class = forms.DeleteTicketForm
    template_name = 'litreview/ticket_delete.html'

    def get(self, request, ticket_id):
        """
        Manage GET request and render ticket deleted page if user allow to
        modify it, else, denied access of this page.

        Parameters:
        - request(HttpRequest): Request object.
        - ticket_id(int): ID of ticket to be deleted.

        Returns:
        - HttpResponse: Render ticket deleted page with context data.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)
        if ticket.author != request.user:
            return HttpResponseForbidden(
                "Accés refusée, vous n'êtes pas l'auteur de cette critique.")
        delete_form = self.delete_form_class()
        context = {
            'delete_form': delete_form,
            'ticket': ticket
        }
        return render(request, self.template_name, context=context)

    def post(self, request, ticket_id):
        """
        Manage POST request, delete the ticket and redirect to home page.

        Parameters:
        - request(HttpRequest): Request object.
        - ticket_id(int): ID of ticket to be modified.

        Returns:
        - HttpResponse: Rendered ticket deleted page with form if it not
        valid, else, delete ticket object then redirect to home page.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)
        delete_form = self.delete_form_class()

        if 'delete_ticket' in request.POST:
            delete_form = self.delete_form_class(request.POST)
            if delete_form.is_valid():
                ticket.delete()
                return redirect('home')

        context = {
            'delete_form': delete_form
        }
        return render(request, self.template_name, context=context)


class ReviewCreate(LoginRequiredMixin, View):
    """
    View for displaying page to create review and ticket in the same time.

    Display form must be completed for create review and ticket in the same
    time.

    Attributes:
    - review_form_class(class): form class of creation review.
    - ticket_form_class(class): form class of creation ticket.
    - template_name(str): Template used for rendering review creation page.

    Methods:
    - get(request): Manage GET request and render review creation page with
    form.
    - post(request): Manage POST request and save review and ticket has been
    create then redirect to home page if form is valid.
    """
    review_form_class = forms.ReviewForm
    ticket_form_class = forms.TicketForm
    template_name = 'litreview/review_create.html'

    def get(self, request):
        """
        Manage GET request and render review creation page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Render review creation page with forms.
        """
        review_form = self.review_form_class()
        ticket_form = self.ticket_form_class()
        context = {
            'review_form': review_form,
            'ticket_form': ticket_form,
        }
        return render(request, self.template_name,
                      context=context)

    def post(self, request):
        """
        Manage POST request, save review and ticket then redirect to home page
        if forms is valid.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Rendered review creation page with forms if it not
        valid, else, save review and ticket objects then redirect to home
        page.
        """
        review_form = self.review_form_class(request.POST)
        ticket_form = self.ticket_form_class(request.POST, request.FILES)

        if all([review_form.is_valid(), ticket_form.is_valid()]):
            ticket = ticket_form.save(commit=False)
            ticket.author = request.user
            ticket.save()
            review = review_form.save(commit=False)
            review.author = request.user
            review.ticket = ticket
            review.save()
            return redirect('home')

        context = {
            'review_form': self.review_form_class(),
            'ticket_form': self.ticket_form_class(),
        }
        return render(request, self.template_name,
                      context=context)


class ReviewUpdate(LoginRequiredMixin, View):
    """
    View for displaying page to modify an review.

    Display form must be completed to modify review.

    Attributes:
    - edit_form_class(class): form class of review.
    - template_name(str): Templated used for rendering review modify page.

    Methods:
    - get(request, review_id): Manage GET request and render review modify
    page with form.
    - post(request, review_id): Manage POST request, save review has been
    modify and redirect to home page if form is valid.
    """
    edit_form_class = forms.ReviewForm
    template_name = 'litreview/review_update.html'

    def get(self, request, review_id):
        """
        Manage GET request and render review modify page if user allow to
        modify it, else, denied access of this page.

        Parameters:
        - request(HttpRequest): Request object.
        - review_id(int): ID of review to be modified.

        Returns:
        - HttpResponse: Render review modify page with form.
        """
        review = get_object_or_404(models.Review, id=review_id)
        if review.author != request.user:
            return HttpResponseForbidden(
                "Accés refusée, vous n'êtes pas l'auteur de cette critique.")
        edit_form = self.edit_form_class(instance=review)
        context = {'edit_form': edit_form,
                   'review': review}
        return render(request, self.template_name, context=context)

    def post(self, request, review_id):
        """
        Manage POST request, save modification of review and redirect to home
        page if form is valid.

        Parameters:
        - request(HttpRequest): Request object.
        - review_id(int): ID of review to be modified.

        Returns:
        - HttpResponse: Rendered review modify page with the form if it not
        valid, else, save modification of review object then redirect to home
        page.
        """
        review = get_object_or_404(models.Review, id=review_id)
        edit_form = self.edit_form_class(instance=review)

        if 'edit_review' in request.POST:
            edit_form = self.edit_form_class(request.POST, instance=review)
            if edit_form.is_valid():
                edit_form.save()
                return redirect('home')

        context = {'edit_form': edit_form}

        return render(request, self.template_name, context=context)


class ReviewDelete(LoginRequiredMixin, View):
    """
    View for displaying page to delete review.

    Display review which will be deleted.

    Attributes:
    - delete_form_class(class): form class to delete review.
    - template_name(str): Templated used for review deleted page.

    Methods:
    - get(request, review_id): Manage GET request and render review deleted
    page with form.
    - post(request, review_id): Manage POST request and delete review then to
    redirect home page.
    """
    delete_form_class = forms.DeleteReviewForm
    template_name = 'litreview/review_delete.html'

    def get(self, request, review_id):
        """
        Manage GET request and render review deleted page if user allow to
        modify it, else, denied access of this page.

        Parameters:
        - request(HttpRequest): Request object.
        - review_id(int): ID of review to be deleted.

        Returns:
        - HttpResponse: Render review deleted page with context data.
        """
        review = get_object_or_404(models.Review, id=review_id)
        if review.author != request.user:
            return HttpResponseForbidden(
                "Accés refusée, vous n'êtes pas l'auteur de cette critique.")
        delete_form = self.delete_form_class()
        context = {
            'delete_form': delete_form,
            'review': review
        }
        return render(request, self.template_name, context=context)

    def post(self, request, review_id):
        """
        Manage POST request, delete the review and redirect to home page.

        Parameters:
        - request(HttpRequest): Request object.
        - review_id(int): ID of review to be deleted.

        Returns:
        - HttpResponse: Rendered review deleted page with form if it not
        valid, else, delete review object then redirect to home page.
        """
        review = get_object_or_404(models.Review, id=review_id)
        delete_form = self.delete_form_class()

        if 'delete_review' in request.POST:
            delete_form = self.delete_form_class(request.POST)
            if delete_form.is_valid():
                review.delete()
                return redirect('home')

        context = {
            'delete_form': delete_form
        }
        return render(request, self.template_name, context=context)


class FollowUsers(LoginRequiredMixin, View):
    """
    View for displaying page of follows.

    Display following user, followed user, user blocked and form to search
    other user for followed this.

    Attributes:
    - template_name(str): Templated used for rendering follows page.

    Methods:
    - get(request): Manage GET request and render follows page with context
    data.
    - post(request): Manage POST request, if form is valid, clean input of
    user in form and search matching user with it, if no matching found,
    display message, else, create instance of object UserFollows.
    """
    template_name = 'litreview/follows.html'

    def get(self, request):
        """
        Manage GET request and render follows page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Render follows page with context data.
        """
        followings = models.UserFollows.objects.filter(follower=request.user)
        followers = models.UserFollows.objects.filter(following=request.user)
        follow_form = forms.FollowForm()
        blocked_users = request.user.blocked_users.all()
        context = {
            'followings': followings,
            'followers': followers,
            'follow_form': follow_form,
            'blocked_users': blocked_users
        }
        return render(request, self.template_name, context=context)

    def post(self, request):
        """
        Manage POST request, if form is valid, clean input of
        user in form and search matching user with it, if no matching found,
        display message, else, create instance of object UserFollows and
        redirect to follows page.

        Parameters:
        - request(HttpRequest): Request object.

        Returns:
        - HttpResponse: Rendered follows page with error message if form not
        valid, else, redirect follows page.
        """
        follow_form = forms.FollowForm(request.POST)
        if follow_form.is_valid():
            username = follow_form.cleaned_data['username']
            try:
                user_follow = User.objects.get(username=username)
                if not models.UserFollows.objects.filter(
                        follower=request.user,
                        following=user_follow).exists():
                    models.UserFollows.objects.create(
                        follower=request.user, following=user_follow)
            except User.DoesNotExist:
                follow_form.add_error('username', "Utilisateur non trouvé")
        else:
            followings = models.UserFollows.objects.filter(
                follower=request.user)
            followers = models.UserFollows.objects.filter(
                following=request.user)
            context = {
                'followings': followings,
                'followers': followers,
                'follow_form': follow_form
            }
            return render(request, self.template_name, context=context)
        return redirect('follows')


class UnfollowUser(LoginRequiredMixin, View):
    """
    View to manage actions of unfollow user.

    This view deleted UserFollow instance between user connected and
    specified user.

    Methods:
    - post(request, user_id): Manage POST request to unfollow user, delete
    UserFollows instance between user connected and specified user.

    Parameters:
    - request(HttpRequest): Request object.
    - user_id(int): ID of user to unfollow.

    Returns:
    - HttpResponseRedirect: Redirect to follows page after unfollowing user.
    """
    def post(self, request, user_id):
        user_unfollow = get_object_or_404(User, id=user_id)
        models.UserFollows.objects.filter(
            follower=request.user, following=user_unfollow).delete()
        return redirect('follows')


class Posts(LoginRequiredMixin, View):
    """
    View for displaying post page with ticket and review created by user
    connected.

    Attributes:
    - template_name(str): Template used for rendering post page.

    Methods:
    - get(request, username): Manage GET request and render post page.
    """
    template_name = 'litreview/user_posts.html'

    def get(self, request, username):
        """
        Manage GET request and render post page.

        This method retrieves tickets and reviews of user connected, it
        annotate each element with type of content.

        Parameters:
        - request(HttpRequest): Request Object.
        - username(str): Username of the user whose posts are to be displayed.

        Returns:
        - HttpResponse: Render post page with context data.
        """
        user = get_object_or_404(User, username=username)
        user_connected = request.user

        tickets_all = models.Ticket.objects.all()

        review_ticket = {
            ticket.id: models.Review.objects.filter(ticket=ticket).exists()
            for ticket in tickets_all
        }

        ticket = models.Ticket.objects.filter(author=user)
        review = models.Review.objects.filter(author=user)

        tickets = ticket.annotate(content_type=Value('TICKET', CharField()))
        reviews = review.annotate(content_type=Value('REVIEW', CharField()))

        posts = sorted(
            chain(tickets, reviews),
            key=lambda post: post.time_created,
            reverse=True
        )

        owner_profile = (user == user_connected)

        context = {
            'user': user,
            'posts': posts,
            'owner_profile': owner_profile,
            'review_ticket': review_ticket
        }
        return render(request, self.template_name, context=context)


class ReviewAnswer(LoginRequiredMixin, View):
    """
    View for displaying review answer page.

    Displays the ticket instance to which the user will create review.

    Attributes:
    - template_name(str): Template used for rendering review answer page.
    - review_form_class(class): form class of create review in answer of
    ticket instance.

    Methods:
    - get(request, ticket_id): Manage GET request and render review answer
    page with form if no review already exist for this ticket instance.
    - post(request, ticket_id): Manage POST request, create review matching
    with ticket instance if form is valid.
    """
    template_name = 'litreview/review_answer.html'
    review_form_class = forms.ReviewForm

    def get(self, request, ticket_id):
        """
        Manage GET request and render review answer page with form if
        no review already exist for the ticket instance.

        Parameters:
        - request(HttpRequest): Request object.
        - ticket_id(int): ID of ticket to be answered.

        Returns:
        - HttpResponse: Render review answer page with context data if no
        review already exist for this ticket, else, redirect to home page.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)
        review_form = self.review_form_class()

        if models.Review.objects.filter(ticket=ticket).exists():
            return redirect('home')

        context = {
            'ticket': ticket,
            'form': review_form,
        }
        return render(request, self.template_name, context=context)

    def post(self, request, ticket_id):
        """
        Manage POST request, save ticket and redirect to home page.

        Paramaters:
        - request(HttpRequest): Request object.
        - ticket_id(int): ID of ticket to be answered.

        Returns:
        - HttpResponseRedirect: Redirect to home page if form is valid, else
        rendered review answer page.
        """
        ticket = get_object_or_404(models.Ticket, id=ticket_id)
        review_form = self.review_form_class(request.POST)

        if models.Review.objects.filter(ticket=ticket).exists():
            return redirect('home')

        if review_form.is_valid():
            review = review_form.save(commit=False)
            review.ticket = ticket
            review.author = request.user
            review.save()
            return redirect('home')
        context = {
            'ticket': ticket,
            'form': review_form
        }
        return render(request, self.template_name, context=context)


class BlockedUser(LoginRequiredMixin, View):
    """
    View for blocking a user.

    Attributes:
    - block_user_class(class): Form class used for blocking user.

    Methods:
    - post(request, user_blocked_id): Manage POST request for blocking a user,
    add the specified user to the blocked user list of the user connected.
    """
    block_user_class = forms.BlockUserForm

    def post(self, request, user_blocked_id):
        """
        Manage POST request to block user.

        Add the specified user to the blocked user list of the user connected.

        Parameters:
        - request(HttpRequest): Request object.
        - user_blocked_id(int): ID of user to be blocked.

        Returns:
        - HttpResponseRedirect: redirect to the follows page after blocking
        user.
        """
        user_blocked = get_object_or_404(User, id=user_blocked_id)
        request.user.blocked_users.add(user_blocked)
        return redirect('follows')


class UnblockedUser(LoginRequiredMixin, View):
    """
    View for unblocking user.

    Attributes:
    - unblock_user_class(class): Form class used for unblocking user.

    Methods:
    - post(request, user_unblocked_id): Manage POST request to unblock user,
    remove the specified user from blocked users list to the user connected.
    """
    unblock_user_class = forms.UnblockedUserForm

    def post(self, request, user_unblocked_id):
        """
        Manage POST request to unblock user.

        Remove the specified user from blocked users list to the user
        connected.

        Parameters:
        - request(HttpRequest): Request object.
        - user_unblocked_id(int): ID of user to be unblocked.

        Returns:
        - HttpResponseRedirect: Redirect to follows page after unblocking
        user.
        """
        user_unblocked = get_object_or_404(User, id=user_unblocked_id)
        request.user.blocked_users.remove(user_unblocked)
        return redirect('follows')

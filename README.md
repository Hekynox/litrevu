LITReview
=========

# Description de LITReview

LiTReview est une application web destiné à la publication et lecture de critiques de livres ou d'articles.

L'application permet de :
* Publier une critique de livres ou d'articles
* Demander une/des critiques d'un livre ou article en particulier
* Rechercher un livre ou article intéressant à lire, en se basant sur les critiques des autres

## Les billets

L'application comprend un système de billets permettant de publier une critique.

Qu'est ce qu'un billet ?
------------------------

Un billet correspond à une demande de critique de la part d'un utilisateur.

Comment ça fonctionne ?
-----------------------

Il sera possible de créer un billet pour un livre ou un article et de rédiger une critique sur ce même billet.

Une fois ceci fait, il sera possible pour les utilisateurs qui suivent l'utilisateur ayant créé le billet de poster une critique en réponse à ce même billet, ou tout autre billet créé au préalable par cet utilisateur.

## Flux (ou fil d'actualité)

Le flux (ou fil d'actualité) est une page similaire à celles de réseaux sociaux tel que Twitter, Facebook ou encore Instagram, lorsque l'utilisateur se connecte sur la plateforme c'est sur cette page qu'il sera redirigé en premier, celle ci affiche : 

* Les billets et les avis de tous les utilisateurs suivis par l'utilisateur connecté
* Ses propres billets et avis
* Les avis en réponses aux billets de l'utilisateur connecté
* Une option permettant de créer un billet, ainsi qu'une option permettant de créer une critique en même temps qu'un billet

Le flux est ordonné par ordre antéchronologique (les plus récents en premier).

## Suivre d'autres utilisateurs

Comme mentionné precedemment, un utilisateur aura la possibilité de suivre d'autres utilisateurs, tout comme il pourra être suivi par d'autres utilisateurs.

Pour cela, une fonctionnalité permet de pouvoir rechercher un utilisateur à l'aide d'un champ à remplir en indiquant le nom de l'utilisateur que l'utilisateur connecté souhaite suivre.

Une page listant tous les utilisateurs suivis par l'utilisateur connecté est également mis à disposition, avec la possibilité de pouvoir cliquer sur une option afin de ne plus suivre un utilisateur en particulier.

## Accès à l'application

Un utilisateur non authentifié de ne pourra pas accéder à l'application, il y aura donc 2 possibilités :
1. L'utilisateur est déjà inscrit à la plateforme, il lui suffit donc de cliquer sur "se connecter" et d'entrer son nom d'utilisateur et mot de passe afin d'accéder à la plateforme.
2. L'utilisateur n'est pas inscrit à la plateforme, il lui faudra donc procéder à l'inscription sur celle ci via un bouton "s'inscrire".

## Résumé des fonctionnalités accessibles par un utilisateur connecté

* Consulter son flux
* Créer des nouveaux billets, ainsi qu'une critique sur ce même billet si souhaité
* Voir, modifier et supprimer ses propres billets ou critiques
* Suivre d'autres utilisateurs
* Voir les utilisateurs qu'il suit
* Arrêter de suivre ou bloquer un utilisateur

# Installation du projet et démarrage de l'application

Commencez par clôner le projet : 

```
git clone https://gitlab.com/Hekynox/litrevu.git
```

Placez vous dans le répertoire du projet que vous venez d'importer, puis créer votre environnement virtuel et activez le : 

**Windows**
```
cd litrevu
python -m venv env
./env/Scripts/activate
```

**Linux**
```
cd litrevu
python3 -m venv env
source env/bin/activate
```

Installer les paquets nécessaire :
```
pip install -r requirements.txt
```

Placez vous dans le projet et démarrez le serveur :
```
cd litrevusite
python manage.py runserver
```

Une fois le serveur démarré, vous pouvez accéder au site ou à l'interface d'administration (Il est nécessaire d'avoir un compte administrateur pour accéder à l'interface d'administration):

Pour accéder au site : http://localhost:8000/

Pour accéder à l'interface d'administration : http://localhost:8000/admin/

## Instruction pour créer un compte administrateur

Placez vous dans le projet si vous n'y êtes pas déjà :

```
cd litrevu/litrevusite
python.exe manage.py createsuperuser
```

Puis suivez les instructions affichés.